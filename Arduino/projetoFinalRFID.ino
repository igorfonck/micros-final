#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h>
//#include <LiquidCrystal_I2C.h>
#include <Keypad.h>

#define PIN_RELE A3

#define SS_PIN 10
#define RST_PIN A2
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

//Keypad
const byte qtdLinhas = 4; //QUANTIDADE DE LINHAS DO TECLADO
const byte qtdColunas = 4; //QUANTIDADE DE COLUNAS DO TECLADO
String senha = "";
//const String SENHA_ESPERADA = "1234";

//CONSTRUÇÃO DA MATRIZ DE CARACTERES
char matriz_teclas[qtdLinhas][qtdColunas] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte PinosqtdLinhas[qtdLinhas] = {7,6,5,4}; //PINOS UTILIZADOS PELAS LINHAS
byte PinosqtdColunas[qtdColunas] = {3,2,8,9}; //PINOS UTILIZADOS PELAS COLUNAS
Keypad meuteclado = Keypad(makeKeymap(matriz_teclas), PinosqtdLinhas, PinosqtdColunas, qtdLinhas, qtdColunas);


void setup(){
  Serial.begin(9600);   // Inicia a serial
  SPI.begin();      // Inicia  SPI bus
  mfrc522.PCD_Init();   // Inicia MFRC522
  Serial.println("Aproxime o seu cartao do leitor...");
  Serial.println();
  //lcd.begin (16,2);
  pinMode(PIN_RELE, OUTPUT);
}


void loop() {
  // Look for new cards
  
  char tecla_pressionada = meuteclado.getKey(); //VERIFICA SE ALGUMA DAS TECLAS FOI PRESSIONADA
  if (tecla_pressionada){ //SE ALGUMA TECLA FOR PRESSIONADA, FAZ
   
    switch(tecla_pressionada)
    {
      case 'A':
      case 'B':
      case 'C':
      case 'D':
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
            //concatena o novo símbolo a senha que estamos digitando
            senha+=tecla_pressionada; 
            //Serial.print(senha);
            break;
      //caso a tecla CLEAR tenha sido pressionada      
      case '*':
              //limpa a variável que guarda a senha que está sendo digitada
              senha = "";
              break;
      //caso a tecla ENTER seja pressionada, devemos comparar as senhas
      case '#':
              Serial.print(" ");
              Serial.print(senha);
              senha = "";
              break;
      default: break;
    
   }
  }
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }
  //Mostra UID na serial
  //Serial.print("UID da tag:");
  String conteudo= "";
  byte letra;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     conteudo.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     conteudo.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  //Serial.println();
  conteudo.toUpperCase();
  delay(3000);
}
