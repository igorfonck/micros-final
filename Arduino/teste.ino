#include <SPI.h>
#include <LiquidCrystal_I2C.h>
#include <Ethernet.h>
#include <string.h> 
#include<stdlib.h>
#define PIN_RELE 5

LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7,3, POSITIVE); // Inicializa o display no endereco 0x27
int LED = 13;
String senha;
char senhaDecod[] = "";
char resposta[] = "";
int var = 0;
char Temp[10];

//ethernet
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress server(192,168,1,2); // Número IP do WebService
IPAddress ip(192,168,1,177); //ip
EthernetClient client;

void ligaFechadura(){
  
  //Serial.println("Liga RELÉ");
  digitalWrite(PIN_RELE, HIGH);
  delay(3000);
  digitalWrite(PIN_RELE, LOW);
  mensageminicial();
  
}

void mensageminicial()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Insira a senha");
  lcd.setCursor(0, 1);
  lcd.print("Aprox. o cartao");
  
}

void acessoPermitido() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Acesso permitido...");
  lcd.setCursor(0, 1);
  lcd.print("Seja bem-vindo!");
  senha = "";
  ligaFechadura();
  delay(3000);
  lcd.clear();
  mensageminicial();
}

void acessoNegado() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Acesso negado!");
  lcd.setCursor(0, 1);
  //lcd.print(senha);
  senha = "";
  delay(3000);
  lcd.clear();
  mensageminicial();
}

void setup(){ 
  Serial.begin(9600); 
  pinMode(LED, OUTPUT);
  //SPI.begin(); 
  lcd.begin (16,2);
  mensageminicial();

  //ethernet
  byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
  IPAddress server(192,168,1,2); // Número IP do WebService
  IPAddress ip(192,168,1,177); //ip
  EthernetClient client;

  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
      // no point in carrying on, so do nothing forevermore:
      // try to congifure using IP address instead of DHCP:
     Ethernet.begin(mac, ip);
    }
   delay(1000);
   Serial.println(ip);
   Serial.println("connecting...");   
} 

void loop()  { 
   
  //ethernet
   client.stop();
   Ethernet.begin(mac,ip);
   
  if (Serial.available())  { 
      Serial.println("Serial.available");

      char aux[2];
     
      senha = Serial.readString();
      for(int i=1; i < senha.length();i++){
        aux[0] = senha[i];
        aux[1] = '\0';
        if(senha[i] == ' '){
          strcat(senhaDecod, "%20");
        }else{
          strcat(senhaDecod, aux);
        }
      }
      String senhaDecodString = senhaDecod;
      Serial.print("Buscando no servidor: ");
      Serial.print(senhaDecod);
      Serial.println();

      // if you get a connection, report back via serial:
      if (client.connect(server, 80)) {
        Serial.println("connected");
        // Make a HTTP request:
        client.print("GET /micros-final/?idTag=");
        client.print(senhaDecodString);
        client.println(" HTTP/1.1");
        client.println("Host: 198.168.1.2");
        client.println("Connection: close");
        //client.println("Content-Type: application/x-www-form-urlencoded");
        //client.println("Content-Length: 250");
        client.println();
      } else {
        Serial.println("connection failed");
      }
      
      // an http request ends with a blank line
      boolean currentLineIsBlank = true;
      boolean leu2 = false;
      boolean leu9 = false;

     while (client.connected()) {
      //Serial.println("While: Cliente conectado");
      if (client.available()) {
        char c = client.read();
        Serial.print(c);

        if(c == '2') {
          leu2 = true;
        } else if (c == '9') {
          if (leu2) {
            leu9 = true;
          }
        } else if (c == '5') {
          if (leu9) {
            var = 1;
          } else {
            leu2 = false;
          }
        } else {
          leu2 = false;
          leu9 = false;
        }

        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        } 
      }
     }
     // give the web browser time to receive the data
     delay(1);
     // close the connection:
     client.stop();
      
      // if the server's disconnected, stop the client:
      if (!client.connected()) {
        Serial.println();
        Serial.println("disconnecting.\n");
        senhaDecod[0] = '\0';

        //Serial.println("\n**** RESPOSTa ***\n");
        //Serial.println(resposta);
        //Serial.println("\n**** ***\n");
      }
      //delay(20000);
     
     if(var == 1){ 
        acessoPermitido();
        var = 0;     
     } else { 
        acessoNegado();
     }
  }
}
