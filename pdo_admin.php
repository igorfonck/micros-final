<?php

if (isset($_POST["salvar"])) {

	//cria conexão
	try {
		  $pdo = new PDO('mysql:host=localhost;dbname=micros', "root", "");
		  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		  $stmt = $pdo->prepare('UPDATE tag SET nome = :nome, nivel = :nivel WHERE id = :id');
		  $stmt->execute(array(
		    ':id'   => $_POST["idTag"],
		    ':nome' => utf8_decode($_POST["tfNome"]),
		    ':nivel' => $_POST["cbNivel"]
		  ));

		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
	}

}

if (isset($_POST["excluir"])) {

	//cria conexão
	try {
		  $pdo = new PDO('mysql:host=localhost;dbname=micros', "root", "");
		  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		  $stmt = $pdo->prepare('DELETE FROM tag WHERE id = :id');
		  $stmt->bindParam(':id', $_POST["idTag"]); 
		  $stmt->execute();

		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
	}

}

if (isset($_POST["nova"])) {

	//cria conexão
	try {
		  	$pdo = new PDO('mysql:host=localhost;dbname=micros', "root", "");
		  	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		  	$stmt = $pdo->prepare('INSERT INTO tag (cod_hex, nome, nivel) VALUES(:cod_hex, :nome, :nivel)');
			$stmt->execute(array(
			':cod_hex' => $_POST['tfHexTag'],
			':nome' => utf8_decode($_POST["tfNome"]),
			':nivel' => $_POST["cbNivel"]
			));

		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
	}

}

?>