	<?php include("./header.php"); ?>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img src="http://placehold.it/300x60?text=Logo" width="150" height="30" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="./index.php">Registros</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">Gerenciar tags</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <h1 class="mt-5">Gerenciamento de Tags Cadastradas</h1>
      <p>Lista de tags cadastradas.</p>

		<!-- Modal para cadastrar nova -->
		<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#novaModal">Cadastrar nova tag</button>
		<div class="modal fade" id="novaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Nova tag</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<div class="container">

				 <form action="pdo_admin.php" method="POST" id="form-modal-nova" target="hiddenFrame">
				  <div class="row modal-row">
				    <div class="col">
				      <b>Nome:</b>
				    </div>
				    <div class="col">
				      <input class="form-control" type="text" name="tfNome" required>
				    </div>
				  </div>
				  <div class="row modal-row">
				    <div class="col">
				      <b>ID da tag:</b>
				    </div>
				    <div class="col">
				      <input class="form-control" type="text" placeholder="AA AA AA AA" name="tfHexTag" required>
				    </div>
				  </div>
				  <div class="row modal-row">
				    <div class="col">
				      <b>Nível:</b>
				    </div>
				    <div class="col">
				      	<select class="form-control" name="cbNivel">';
					      <option value="0">0 - Sem acesso</option>
					      <option value="1" selected>1 - Administrador</option>';
					    </select>
				    </div>
				  </div>
				  <input type="hidden" name="idTag" value="'.$linha["id"].'">
				  </form>

				</div>							
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="submit" class="btn btn-success" name="nova" form="form-modal-nova">Salvar</button>
			  </div>
			</div>
		  </div>
		</div>
      
      <?php
      //Cria conexão
      try {
          $pdo = new PDO('mysql:host=localhost;dbname=micros', "root", "");
          $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
          echo 'Error: ' . $e->getMessage();
      }

      //Mostra a tabela
      $sql = "SELECT * FROM tag";
      $result = $pdo->query($sql);

      if ($result) {
		  
        ?>
        <table id="minhaTabela" style="width:100%">
        <thead>
            <tr>
              <th>ID</th>
              <th>Nome</th>
              <th>Código</th>
              <th>Nível</th>
			  <th>Ações</th>
            </tr>
        </thead>
        <tbody>
        <?php

		  $cont = 0;
          // output data of each row
          while ($linha = $result->fetch(PDO::FETCH_ASSOC)) {
              echo "<tr><td>" . $linha["id"]. "</td><td>" . utf8_encode($linha["nome"]) . "</td><td>" . $linha["cod_hex"]. "</td><td>";
              if($linha["nivel"] == 0)
               	echo "0 - Sem acesso";
           	  else
           	  	echo "1 - Administrador";
              echo "</td><td style='text-align:center'>".'<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#editModal'.$cont.'">Editar</button>'."</td></tr>";
			  
			  echo '<div class="modal fade" id="editModal'.$cont.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Editar tag</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							<div class="container">

							<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
							  <strong>As alterações foram salvas!</strong> Atualize a página para vizualizar na tabela. <a href="#">Atualizar</a>
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							  </button>
							</div>

							 <form action="pdo_admin.php" method="POST" id="form-modal-'.$cont.'" target="hiddenFrame">
							  <div class="row modal-row">
							    <div class="col">
							      <b>Nome:</b>
							    </div>
							    <div class="col">
							      <input class="form-control" type="text" value="'.utf8_encode($linha["nome"]).'" name="tfNome" required>
							    </div>
							  </div>
							  <div class="row modal-row">
							    <div class="col">
							      <b>Nível:</b>
							    </div>
							    <div class="col">
							      	<select class="form-control" name="cbNivel">';
								      if($linha["nivel"] == 0)
								      	echo '<option value="0" selected>0 - Sem acesso</option><option value="1">1 - Administrador</option>';
								      else
								      	echo '<option value="0">0 - Sem acesso</option><option value="1" selected>1 - Administrador</option>';
								    echo '</select>
							    </div>
							  </div>
							  <input type="hidden" name="idTag" value="'.$linha["id"].'">
							  </form>

							</div>							
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
							<button type="submit" class="btn btn-danger" name="excluir" form="form-modal-'.$cont.'">Excluir tag</button>
							<button type="submit" class="btn btn-success" name="salvar" form="form-modal-'.$cont.'">Salvar alterações</button>
						  </div>
						</div>
					  </div>
					</div>';
			  $cont++;
          }

          ?>
          </tbody>
        </table>
        <?php

      } else {
          echo "0 results";
      }
      ?>
      
    </div>
    <!-- /.container -->

    <iframe name="hiddenFrame" width="0" height="0" border="0" style="display: none;"></iframe>

    <?php include("./footer.php"); ?>