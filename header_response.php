<?php

//cria conexão

try {
	  $pdo = new PDO('mysql:host=localhost;dbname=micros', "root", "");
	  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
	  echo 'Error: ' . $e->getMessage();
}

if (isset($_GET['idTag'])) {

    //consulta tag no banco
    $consulta = $pdo->query("SELECT * FROM tag WHERE cod_hex = '".$_GET['idTag']."';");
	while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
	    $resultConsultaID = $linha['id'];
	    $resultConsultaNivel = $linha['nivel'];
	}

	if(!isset($resultConsultaID)) {
		//Não encontrou a tag!
		header("HTTP/1.0 299 NEGADO");
	}
	else {
		//Encontrou a tag...

		//Verifica nível da tag...
		if($resultConsultaNivel == 0) {
			//Não possui acesso!
			header("HTTP/1.0 299 NEGADO");
		}
		else {
			//Possui acesso!
			header("HTTP/1.0 295 PERMITIDO");
		}
	}
}

?>