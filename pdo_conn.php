<?php

//cria conexão
try {
	  $pdo = new PDO('mysql:host=localhost;dbname=micros', "root", "");
	  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
	  echo 'Error: ' . $e->getMessage();
}

if (isset($_GET['idTag'])) {

    //consulta tag no banco
    $consulta = $pdo->query("SELECT * FROM tag WHERE cod_hex = '".$_GET['idTag']."';");
	while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
	    $resultConsultaID = $linha['id'];
	    $resultConsultaNivel = $linha['nivel'];
	}

	if(!isset($resultConsultaID)) {
		//Não encontrou a tag... Cria.
		try {
	     
		  	$stmt = $pdo->prepare('INSERT INTO tag (cod_hex, nome, nivel) VALUES(:cod_hex, :nome, :nivel)');
			$stmt->execute(array(
			':cod_hex' => $_GET['idTag'],
			':nome' => 'Novo acesso',
			':nivel' => 0
			));

			$resultConsultaID = $pdo->lastInsertId();

			//nível 0, sem acesso

			echo "Esta tag não possui acesso! Solicite a um administrador.<br>";
			echo "ID da tag: ".$_GET['idTag'];

		} catch(PDOException $e) {
		  	echo 'Error: ' . $e->getMessage();
		}
	}
	else {
		//Encontrou a tag

		//Verifica nível da tag
		if($resultConsultaNivel == 0) {
			//Não possui acesso
			echo "Esta tag não possui acesso! Solicite a um administrador.<br>";
			echo "ID da tag: ".$_GET['idTag'];
		}
		else {
			//Possui acesso. Registra.

			try {
		     
			  	$stmt = $pdo->prepare('INSERT INTO registro (fk_Tag_id) VALUES(:id)');
				$stmt->execute(array(
				':id' => $resultConsultaID
				));

			} catch(PDOException $e) {
			  	echo 'Error: ' . $e->getMessage();
			}

			//Mostra a tabela
			$sql = "SELECT hora, nome, cod_hex, nivel, tag.id as id_tag, registro.id as id_registro FROM registro, tag WHERE registro.fk_Tag_id = tag.id";
			$result = $pdo->query($sql);

			if ($result) {

				?>
				<table id="minhaTabela" style="width:100%">
				<thead>
					  <tr>
					  	<th>ID</th>
					    <th>Data</th>
						<th>Hora</th>
					    <th>Nome</th>
					    <th>Tag</th>
					  </tr>
				</thead>
				<tbody>
				<?php

			    // output data of each row
			    while ($linha = $result->fetch(PDO::FETCH_ASSOC)) {
					//$data = date('M j Y', strtotime($linha["hora"]));
					$data = date('d/m/Y', strtotime($linha["hora"]));
					$hora = date('g:i A', strtotime($linha["hora"]));
			        echo "<tr><td>" . $linha["id_registro"]. "</td><td>" . $data . "</td><td>" . $hora . "</td><td>" . utf8_encode($linha["nome"]) . "</td><td>" . $linha["cod_hex"]. "</td></tr>";
			    }

			    ?>
			    </tbody>
				</table>
				<?php

			} else {
			    echo "0 results";
			}
		}
		

	}

	

}

?>