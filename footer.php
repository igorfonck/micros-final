<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- DataTable -->
    <link rel="stylesheet" type="text/css" href="DataTables/DataTables-1.10.18/css/jquery.dataTables.css"/>
    <script type="text/javascript" src="DataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>


    <script>
    	$(document).ready( function () {
	    	$('#minhaTabela').DataTable( {
	    		"language": {
                	"url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            	}
	    	});
		} );
	</script>

	<style type="text/css">
		.dataTables_wrapper {
		    margin: 50px 5px 25px;
		}
		.modal-row {
			margin-bottom: 10px;
		}
	</style>

  </body>

</html>
