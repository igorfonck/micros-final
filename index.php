    <?php
    include("./header_response.php");
    include("./header.php");
    ?>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img src="http://placehold.it/300x60?text=Logo" width="150" height="30" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Registros</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./admin.php">Gerenciar tags</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <h1 class="mt-5">Gerenciamento da Fechadura Eletrônica</h1>
      <p>Aproxime a tag RFID do leitor para acessar.</p>

      <?php include "pdo_conn.php"; ?>
      
    </div>
    <!-- /.container -->

    <?php include("./footer.php"); ?>